#!/bin/env python
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory

def TrigMatchingToolCfg(ConfigFlags, TrigDecisionTool ):
    acc = ComponentAccumulator()
    acc.setPrivateTools( CompFactory.Trig.R3MatchingTool( TrigDecisionTool=TrigDecisionTool ) )
    return acc

def PhotonPointingToolCfg(ConfigFlags, photonContianerName):
    acc = ComponentAccumulator()
    acc.setPrivateTools( CompFactory.CP.PhotonPointingTool( isSimulation = ConfigFlags.Input.isMC, ContainerName=photonContianerName) )
    return acc

def PhotonVertexSelectionToolCfg(ConfigFlags, vertexContainerName="PrimaryVertices", photonContianerName="Photons"):
    acc = ComponentAccumulator()
    photonPointingTool = acc.popToolsAndMerge( PhotonPointingToolCfg(ConfigFlags, photonContianerName) )
    acc.setPrivateTools( CompFactory.CP.PhotonVertexSelectionTool(PhotonPointingTool=photonPointingTool, updatePointing=True, VertexContainer=vertexContainerName) )
    return acc

def PrimaryVertexRefittingToolCfg(ConfigFlags, **kwargs):
    acc = ComponentAccumulator() 
    from TrkConfig.TrkVertexFitterUtilsConfig import TrackToVertexIPEstimatorCfg
    trkToIP = acc.popToolsAndMerge( TrackToVertexIPEstimatorCfg(ConfigFlags,**kwargs) )
    kwargs.setdefault( "TrackToVertexIPEstimator", trkToIP )
    acc.setPrivateTools( CompFactory.Analysis.PrimaryVertexRefitter( **kwargs) ) 
    return acc

def GoodRunListToolCfg(ConfigFlags):
    acc = ComponentAccumulator()
    acc.setPrivateTools( CompFactory.GoodRunsListSelectionTool("MyGRLTool", GoodRunsListVec=ConfigFlags.GRL) )
    return acc

def ServiceCfg(ConfigFlags):
    from AthenaConfiguration.MainServicesConfig import MainServicesCfg
    acc = MainServicesCfg(ConfigFlags)
    acc.addService(CompFactory.AthenaEventLoopMgr(EventPrintoutInterval=1000))

    sysSvc = CompFactory.CP.SystematicsSvc("SystematicsSvc")
    acc.addService(sysSvc)

    from GaudiSvc.GaudiSvcConf import THistSvc
    histSvc = CompFactory.THistSvc(Output = ["ANALYSIS DATAFILE='" + ConfigFlags.outfilename + "', OPT='RECREATE'"])
    acc.addService(histSvc)

    from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
    acc.merge(PoolReadCfg(ConfigFlags))
    return acc

def AlgrithmCfg(ConfigFlags, **kwargs):
    acc = ComponentAccumulator()
    
    # # bring back if jets needed
    # from JetRecConfig.JetRecConfig import JetRecCfg
    # from JetRecConfig.StandardSmallRJets import AntiKt4EMTopo
    # acc.merge(JetRecCfg(ConfigFlags, AntiKt4EMTopo))

    # # not clear how to reconstruct met using CA
    # from DerivationFrameworkJetEtMiss.METCommonConfig import METCommonCfg 
    # acc.merge(METCommonCfg(ConfigFlags))

    # define triger decision tool
    from TrigDecisionTool.TrigDecisionToolConfig import TrigDecisionToolCfg
    tdt = acc.getPrimaryAndMerge(TrigDecisionToolCfg(ConfigFlags ))
    tdt.AcceptMultipleInstance = 1
    
    # preselection (GRL, cleaning)
    grltool = acc.popToolsAndMerge( GoodRunListToolCfg(ConfigFlags) )
    acc.addEventAlgo(
        CompFactory.GRLSelectionAlg("GRLSelectionAlg",
        isData=not ConfigFlags.Input.isMC,
        GRLTool=grltool)
    )

    # pileup reweighting
    from AsgAnalysisAlgorithms.PileupAnalysisSequence import makePileupAnalysisSequence
    dataType = "mc"
    lumicalcfiles = [
        "GoodRunsLists/data22_13p6TeV/20230207/ilumicalc_histograms_None_428648-440613_OflLumi-Run3-003.root",
        # "GoodRunsLists/data23_13p6TeV/20230828/ilumicalc_histograms_None_451587-456749_OflLumi-Run3-003_ignoreTRIG_JETCTPIN.root",
    ]
    prwfiles = [
        "PileupReweighting/mc21_common/mc21a.410000.physlite.prw.v1.root"
        # "PileupReweighting/mc23_common/mc23a.410000.physlite.prw.v2.root",
        # "PileupReweighting/mc23_common/mc23c.450000.physlite.prw.v1.root"
    ]
    if not ConfigFlags.Input.isMC:
        dataType = "data"
        lumicalcfiles = []
        prwfiles = []
    pileupSequence = makePileupAnalysisSequence(dataType,
                                                 userPileupConfigs=prwfiles,
                                                 userLumicalcFiles=lumicalcfiles,)
    pileupSequence.configure( inputName = "EventInfo", outputName = "")
    acc.addSequence(CompFactory.AthSequencer(pileupSequence.getName()))
    for alg in pileupSequence.getGaudiConfig2Components():
        # alg.pileupWeightDecoration = "PileupWeight"
        acc.addEventAlgo(alg, pileupSequence.getName())

    # # electron cp
    # from EgammaAnalysisAlgorithms.ElectronAnalysisSequence import makeElectronAnalysisSequence
    # workingpoint = 'LooseLHElectron.Loose_VarRad'
    # electronSequence = makeElectronAnalysisSequence( dataType, workingpoint, postfix = 'loose',
    #                                                  recomputeLikelihood=False, shallowViewOutput = False )
    # electronSequence.configure( inputName = 'Electrons',
    #                             outputName = 'AnaElectrons_%SYS%' )
    # acc.addSequence(CompFactory.AthSequencer(electronSequence.getName()))
    # for alg in electronSequence.getGaudiConfig2Components():
    #     acc.addEventAlgo(alg, electronSequence.getName())

    # select  photons
    acc.addEventAlgo(
        CompFactory.PhotonSelectionAlg("PhotonSelectionAlg",
        ifDoTruthMatching=True,
        isData=not ConfigFlags.Input.isMC,
        TrigDecisionTool=tdt)
    )

    electronsAnalysisContainer="ElectronsAnalysis"
    trigMatchingtool = acc.popToolsAndMerge(TrigMatchingToolCfg(ConfigFlags, tdt))
    # select electrons
    acc.addEventAlgo(
        CompFactory.ElectronSelectionAlg("ElectronSelectionAlg",
        isData=not ConfigFlags.Input.isMC,
        electronsAnalysisContainerName=electronsAnalysisContainer,
        TrigDecisionTool=tdt,
        TrigMatchingTool=trigMatchingtool)
    )

    # refit primary vertex for electrons
    pvRefitter = acc.popToolsAndMerge( PrimaryVertexRefittingToolCfg(ConfigFlags) )
    acc.addEventAlgo(
        CompFactory.ZeeVertexRefitAlg("ZeeVertexRefitAlg",
        PrimaryVertexRefitterTool=pvRefitter,
        RefittedPVContainerName="RefittedPrimaryVertices",
        Electrons=electronsAnalysisContainer,
        isData=not ConfigFlags.Input.isMC)
    )

    # dump variables
    finalPVContainerName = "PrimaryVertices"
    if ConfigFlags.doZeeRefit:
        if not ConfigFlags.isZee:
            print("ERROR: doZeeRefit is set to True, but isZee is set to False.")
            exit(1)
        finalPVContainerName = "RefittedPrimaryVertices"


    if not ConfigFlags.isZee:
        VertexSelectionTool = acc.popToolsAndMerge( PhotonVertexSelectionToolCfg(ConfigFlags, vertexContainerName=finalPVContainerName) )
    else:
        VertexSelectionTool = acc.popToolsAndMerge( PhotonVertexSelectionToolCfg(ConfigFlags, vertexContainerName=finalPVContainerName, photonContianerName=electronsAnalysisContainer) )
    acc.addEventAlgo(
        CompFactory.HyyVertexAnalysisAlg("HyyVertexAnalysisAlg",
                                         photonVertexSelectionTool=VertexSelectionTool,
                                         isZee=ConfigFlags.isZee,
                                         PVContainerName=finalPVContainerName,
                                         Electrons=electronsAnalysisContainer,
                                         isData=not ConfigFlags.Input.isMC)
    )
    return acc

def get_args(ConfigFlags):
    import optparse
    parser = optparse.OptionParser()
    parser.add_option( '-i', '--input-sample', dest = 'input_sample',
                    action = 'store', type = 'string', default = 'input_Sample',
                    help = 'Input AOD file' )
    parser.add_option( '-o', '--output-Sample', dest = 'output_sample',
                    action = 'store', type = 'string', default = 'output_Sample',
                    help = 'Output sample name' )
    parser.add_option( '-z', '--isZee', dest = 'isZee',
                    action = 'store', type = 'int', default = 1,
                    help = 'Process Zee or not' )
    parser.add_option( '-s', '--skipEvents', dest = 'skipEvents',
                    action = 'store', type = 'int', default = None,
                    help = 'skipEvents' )
    parser.add_option( '-f', '--FirstEvent', dest = 'FirstEvent',
                    action = 'store', type = 'int', default = None,
                    help = 'FirstEvent' )
    parser.add_option( '-m', '--MaxEvents', dest = 'MaxEvents',
                    action = 'store', type = 'int', default = None,
                    help = 'MaxEvents' )
    ( options, args ) = parser.parse_args()
    inputfiles = []
    for each_file in options.input_sample.split(','):
        inputfiles.append(each_file)
    ConfigFlags.Input.Files = inputfiles
    ConfigFlags.isZee = options.isZee
    if options.isZee:
        ConfigFlags.doZeeRefit = True
    else:
        ConfigFlags.doZeeRefit = False
    if options.skipEvents is not None:
        ConfigFlags.Exec.SkipEvents = options.skipEvents
    if options.FirstEvent is not None:
        ConfigFlags.Exec.FirstEvent = options.FirstEvent
    if options.MaxEvents is not None:
        ConfigFlags.Exec.MaxEvents = options.MaxEvents
    if options.output_sample is not None:
        ConfigFlags.outfilename = options.output_sample

    return options

def main():
    from AthenaConfiguration.AllConfigFlags import initConfigFlags
    from AthenaConfiguration.TestDefaults import defaultGeometryTags, defaultTestFiles
    from AthenaCommon.Logging import log
    from AthenaCommon.Constants import DEBUG, INFO, ERROR

    # Test setup
    log.setLevel(DEBUG)

    # flags
    ConfigFlags = initConfigFlags()
    ConfigFlags.addFlag("isZee", True)
    ConfigFlags.addFlag("doZeeRefit", True)
    ConfigFlags.addFlag("outfilename", "output.root")
    ConfigFlags.addFlag("GRL", ["GoodRunsLists/data22_13p6TeV/20230207/data22_13p6TeV.periodAllYear_DetStatus-v109-pro28-04_MERGED_PHYS_StandardGRL_All_Good_25ns_ignore_TRIGMUO_TRIGLAR.xml",
                                "GoodRunsLists/data23_13p6TeV/20230828/data23_13p6TeV.periodAllYear_DetStatus-v110-pro31-06_MERGED_PHYS_StandardGRL_All_Good_25ns_ignoreTRIG_JETCTPIN.xml"])
    ConfigFlags.Exec.MaxEvents=-1
    ConfigFlags.MET.DoPFlow = False

    # get args
    get_args(ConfigFlags)

    ConfigFlags.lock()


    # job settings
    acc = ServiceCfg(ConfigFlags)
    acc.merge(AlgrithmCfg(ConfigFlags))

    # finalize
    ConfigFlags.dump()
    acc.printConfig(withDetails = True, summariseProps = True)
    sc = acc.run()
    import sys
    # Success should be 0
    sys.exit(not sc.isSuccess())


if __name__ == "__main__":
    is_successful = main()
    sys.exit(0 if is_successful else 1)

    # pathena --trf "gridrun.py --input-sample=%IN --output-Sample=%OUT.NTUP.root" --inDS mc23_13p6TeV.601189.PhPy8EG_AZNLO_Zee.recon.AOD.e8514_s4162_r14622 --outDS user.tqiu.zjetfirst100files --nfiles 100
    # pathena --trf "gridrun.py --input-sample=%IN --output-Sample=%OUT.NTUP.root" --inDS data22_13p6TeV.periodH.physics_Main.PhysCont.AOD.pro29_v01 --outDS user.tqiu.data22th1
    # prun --exec "gridrun.py --input-sample=%IN" --inDS mc23_13p6TeV.700767.Sh_2214_Zee_maxHTpTV2.recon.AOD.e8514_e8528_s4159_s4114_r14799 --outDS user.tqiu.zjettest2 --nEvents 200 --useAthenaPackages