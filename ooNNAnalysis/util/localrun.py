#!/bin/env python
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory

def ServiceCfg(ConfigFlags):
    from AthenaConfiguration.MainServicesConfig import MainServicesCfg
    acc = MainServicesCfg(ConfigFlags)
    acc.addService(CompFactory.AthenaEventLoopMgr(EventPrintoutInterval=10))

    #sysSvc = CompFactory.CP.SystematicsSvc("SystematicsSvc")
    #acc.addService(sysSvc)

    from GaudiSvc.GaudiSvcConf import THistSvc
    histSvc = CompFactory.THistSvc(Output = ["ANALYSIS DATAFILE='myfile.root', OPT='RECREATE'"])
    acc.addService(histSvc)

    from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
    acc.merge(PoolReadCfg(ConfigFlags))
    return acc

def AlgrithmCfg(ConfigFlags, **kwargs):
    acc = ComponentAccumulator()
    acc.addEventAlgo(
        CompFactory.ooNNAnalysisAlg("ooNNAnalysisAlg")
    )
    return acc

def main():
    from AthenaConfiguration.AllConfigFlags import initConfigFlags
    from AthenaConfiguration.TestDefaults import defaultGeometryTags, defaultTestFiles
    from AthenaCommon.Logging import log
    from AthenaCommon.Constants import DEBUG, INFO, ERROR

    # Test setup
    log.setLevel(DEBUG)

    # flags
    ConfigFlags = initConfigFlags()
    ConfigFlags.Exec.MaxEvents=100
    ConfigFlags.Input.Files = ["/eos/user/m/mumohdso/vbf_cp_study_ab22/run/DAOD_HIGG1D1.32896810._000001.pool.root.1"]

    ConfigFlags.fillFromArgs()
    ConfigFlags.lock()

    # job settings
    acc = ServiceCfg(ConfigFlags)
    acc.merge(AlgrithmCfg(ConfigFlags))

    # finalize
    ConfigFlags.dump()
    acc.printConfig(withDetails = True, summariseProps = True)
    sc = acc.run()
    import sys
    # Success should be 0
    sys.exit(not sc.isSuccess())

if __name__ == "__main__":
    is_successful = main()
    sys.exit(0 if is_successful else 1)
