################################################################################
# Package: ooNNAnalysis
################################################################################
## automatically generated CMakeLists.txt file

# Declare the package
atlas_subdir( ooNNAnalysis )

# Declare external dependencies ... default here is to include ROOT
find_package( ROOT COMPONENTS MathCore RIO Core Tree Hist )
#find_package( Rivet )
#find_package( FastJet )
#find_package( YODA )

# Declare package as a library
# Note the convention that library names get "Lib" suffix
# Any package you depend on you should add
# to LINK_LIBRARIES line below (see the example)
atlas_add_library( ooNNAnalysisLib src/*.cxx
                   PUBLIC_HEADERS ooNNAnalysis
                   #INCLUDE_DIRS ${FASTJET_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS} ${RIVET_INCLUDE_DIRS} ${YODA_INCLUDE_DIRS}
                   #LINK_LIBRARIES ${FASTJET_LIBRARIES} ${ROOT_LIBRARIES} AtlasHepMCLib TruthUtils ${RIVET_LIBRARIES} ${YODA_LIBRARIES} 
                   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
                   LINK_LIBRARIES ${ROOT_LIBRARIES} AtlasHepMCLib TruthUtils
                   AthAnalysisBaseCompsLib 
                   AsgDataHandlesLib 
                   AsgTools 
                   xAODCutFlow
                   xAODEventInfo 
                   xAODEgamma
                   xAODJet
                   HLeptonsCPRWLib
)

# if you add athena components (tools, algorithms) to this package
# these lines are needed so you can configure them in joboptions
atlas_add_component( ooNNAnalysis src/components/*.cxx
                      NOCLIDDB
                      LINK_LIBRARIES 
                      ooNNAnalysisLib  
                      AthenaBaseComps 
                      AsgDataHandlesLib 
                      AsgTools 
                      xAODCutFlow
                      xAODEventInfo 
                      xAODEgamma
                      xAODJet
                      HLeptonsCPRWLib
)

# if you add an application (exe) to this package
# declare it like this (note convention that apps go in the util dir)
# atlas_add_executable( MyApp util/myApp.cxx
#                       LINK_LIBRARIES ooNNAnalysisLib
# )

# Install python modules, joboptions, and share content
atlas_install_python_modules( python/*.py )
# atlas_install_joboptions( share/*.py )
atlas_install_data( data/* )
# You can access your data from code using path resolver, e.g.
# PathResolverFindCalibFile("ooNNAnalysis/file.txt")

atlas_install_scripts(
  util/*
)

