#ifndef OONNANALYSIS_OONNANALYSISALG_H
#define OONNANALYSIS_OONNANALYSISALG_H 1

#include "AthenaBaseComps/AthAlgorithm.h"
#include "xAODEgamma/Photon.h"
#include <vector>
#include "xAODTruth/TruthEventContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODEgamma/EgammaContainer.h"
#include "xAODEgamma/ElectronxAODHelpers.h"
#include <TTree.h>
#include "AthContainers/ConstDataVector.h"
#include "xAODEventInfo/EventInfo.h"

#include "HLeptonsCPRW/OptObsEventStore.h"
#include "xAODJet/Jet.h"
#include "xAODJet/JetContainer.h"

#include "xAODBase/IParticleHelpers.h"
#include "xAODCore/AuxContainerBase.h"
#include "xAODCore/ShallowCopy.h"

class ooNNAnalysisAlg : public ::AthAlgorithm
{
public:
    ooNNAnalysisAlg(const std::string &name, ISvcLocator *pSvcLocator);
    virtual ~ooNNAnalysisAlg();

    /// uncomment and implement methods as required

    // IS EXECUTED:
    virtual StatusCode initialize();     // once, before any input is loaded
    virtual StatusCode beginInputFile(); // start of each input file, only metadata loaded
    virtual StatusCode execute();        // per event
    virtual StatusCode finalize();       // once, after all events processed

    //methods
    static bool comparePt(const xAOD::IParticle *a, const xAOD::IParticle *b);

private:
    //HAWK
    OptObsEventStore ooES; 

    //PDF
    float Q; //!
    int f_q1In; //!
    int f_q2In; //!
    float Bjorkenx1; //!
    float Bjorkenx2; //!

    //truth vars
    float E_truthH; //!
    float Px_truthH; //!
    float Py_truthH; //!
    float Pz_truthH; //!
    float pT_truthQ1; //!
    float pT_truthQ2; //!
    float pT_truthQ3; //!
    float eta_truthQ1; //!
    float eta_truthQ2; //!
    float eta_truthQ3; //!
    float phi_truthQ1; //!
    float phi_truthQ2; //!
    float phi_truthQ3; //!
    float m_truthQ1; //!
    float m_truthQ2; //!
    float m_truthQ3; //!
    std::map<TString, int> Pid_truthQ1; //!
    std::map<TString, int> Pid_truthQ2; //!
    std::map<TString, int> Pid_truthQ3; //!

    //OO vars
    std::map<TString, float> oo1; //!
    std::map<TString, float> oo2; //!
    float oo1_truth; //!
    float oo2_truth; //!
    float WeightDtilde1; //!
    float WeightDtilde2; //!

    //methods
    StatusCode initPDFSet();
    StatusCode getTruthEvents();
    StatusCode computeHAWKWeights();
    //void decorateBJetSelection(xAOD::JetContainer *jets);
    //void decorateCJetSelection(xAOD::JetContainer *jets);
    bool isFinalHiggs(const xAOD::TruthParticle *part);
    void ReweightTruthInfo(const xAOD::TruthParticleContainer *truth_cont, std::vector<xAOD::TruthParticle *> &truth_partons, xAOD::TruthParticle *&truth_higgs);
    const xAOD::TruthParticleContainer* getTruthParticles();
    ConstDataVector<xAOD::TruthParticleContainer> getFinalHiggsBosons(const xAOD::TruthParticleContainer *truthParticles);
    const xAOD::TruthParticleContainer getHiggsBosons();
    xAOD::JetContainer getJets();
    DataVector<xAOD::TruthParticle> getDeepCopyTruthParticle(const DataVector<xAOD::TruthParticle> *parts);

    // define methods
    StatusCode fillEventInfo();
    StatusCode clearVectors();

    // add write handles as private member(s):
    //SG::ReadHandleKey<float> m_electronSFKey{this, "electronSF", "electronSF", "" };
    SG::ReadHandleKey<xAOD::EventInfo> m_eventInKey{
        this, "eventIn", "EventInfo", "containerName to read"};
    SG::ReadHandleKey<xAOD::TruthParticleContainer> m_truthParticleContKey{
        this, "truthParticleIn", "TruthParticles", "containerName to read"};
    SG::ReadHandleKey<xAOD::JetContainer> m_truthJetContKey{
        //this, "truthJetIn", "AntiKt4TruthJets", "containerName to read"};
        this, "truthJetIn", "AntiKt10TruthJets", "containerName to read"};
    SG::ReadHandleKey<xAOD::TruthEventContainer> m_truthEventContKey{
        this, "truthEventsIn", "TruthEvents", "containerName to read"};
    //SG::ReadHandleKey<xAOD::ElectronContainer> m_electronContainerKey{
    //    this, "Electrons", "Electrons", "containerName to read"};
    //SG::ReadHandleKey<xAOD::PhotonContainer> m_photonContainerKey{
    //    this, "Photons", "Photons", "containerName to read"};

    // selected objects
    std::shared_ptr<ConstDataVector<xAOD::PhotonContainer>> m_selectedPhotons;
    std::shared_ptr<ConstDataVector<xAOD::ElectronContainer>> m_selectedElectrons;
    std::shared_ptr<ConstDataVector<xAOD::EgammaContainer>> m_selectedEgammas;

    // add tree and branches
    TTree *m_myTree;
    TTree *m_myTree_meta;
    int m_eventCounter;
    float m_mcWeight;
    float m_mu;
    int m_mcChannelNumber;
    int m_runNumber;
    ULong64_t m_eventNumber;
    float m_PileupWeight;
    float m_finalWeight;
    float m_beamSpotMean;
    float m_beamSpotSigma;

    std::vector<unsigned int> m_selectedElectronIDs;
    std::vector<unsigned int> m_selectedPhotonIDs;

};

#endif //> !OONNANALYSIS_OONNANALYSISALG_H

