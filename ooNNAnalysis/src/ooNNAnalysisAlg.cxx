#include "ooNNAnalysisAlg.h"
#include "GaudiKernel/ITHistSvc.h"
#include "GaudiKernel/ServiceHandle.h"
#include <TTree.h>
#include "xAODTruth/TruthEventContainer.h"

#include <boost/math/distributions.hpp>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/variance.hpp>
#include <boost/accumulators/statistics/kurtosis.hpp>
#include <boost/accumulators/statistics/skewness.hpp>

#include "TMath.h"

// EDM include(s):
#include "xAODBase/IParticleHelpers.h"
#include "xAODJet/Jet.h"
#include "xAODJet/JetContainer.h"

ooNNAnalysisAlg::ooNNAnalysisAlg( const std::string& name, ISvcLocator* pSvcLocator ) : AthAlgorithm( name, pSvcLocator )
{
  //declareProperty( "isData", m_isData = 0);
  //declareProperty( "isZee", m_isZee = 0 );
  //m_outVxColl = "HggPrimaryVertices";
}


ooNNAnalysisAlg::~ooNNAnalysisAlg() {}


StatusCode ooNNAnalysisAlg::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");

  // initialize read handles
  ATH_CHECK(m_eventInKey.initialize());
  ATH_CHECK(m_truthParticleContKey.initialize());
  ATH_CHECK(m_truthJetContKey.initialize());
  ATH_CHECK(m_truthEventContKey.initialize());

  //ATH_CHECK(m_electronSFKey.initialize());

  // retrieve tools
  //ATH_CHECK(m_photonVertexSelectionTool.retrieve());

  // define output
  ServiceHandle<ITHistSvc> histSvc("THistSvc",name()); 
  ATH_CHECK( histSvc.retrieve() );

  m_myTree_meta = new TTree("metadata","metadata");
  ATH_CHECK( histSvc->regTree("/ANALYSIS/metadata",m_myTree_meta) );
  m_myTree_meta->Branch("mcWeight", &m_mcWeight);
  m_myTree_meta->Branch("FinalWeight", &m_finalWeight);
  m_myTree_meta->Branch("PileupWeight", &m_PileupWeight);
  m_myTree_meta->Branch("mu", &m_mu);

  m_myTree = new TTree("data","data");
  ATH_CHECK( histSvc->regTree("/ANALYSIS/data",m_myTree) );

  // define branches
  m_myTree->Branch("mcWeight", &m_mcWeight);
  m_myTree->Branch("mu", &m_mu);
  m_myTree->Branch("mcChannelNumber", &m_mcChannelNumber);
  m_myTree->Branch("runNumber", &m_runNumber);
  m_myTree->Branch("eventNumber", &m_eventNumber);
  m_myTree->Branch("beamSpotZMean", &m_beamSpotMean);
  m_myTree->Branch("beamSpotZSigma", &m_beamSpotSigma);

  m_myTree->Branch("Q", &Q);
  m_myTree->Branch("f_q1In", &f_q1In);
  m_myTree->Branch("f_q2In", &f_q2In);
  m_myTree->Branch("Bjorkenx1", &Bjorkenx1);
  m_myTree->Branch("Bjorkenx2", &Bjorkenx2);
  m_myTree->Branch("E_truthH", &E_truthH);
  m_myTree->Branch("Px_truthH", &Px_truthH);
  m_myTree->Branch("Py_truthH", &Py_truthH);
  m_myTree->Branch("Pz_truthH", &Pz_truthH);
  m_myTree->Branch("pT_truthQ1", &pT_truthQ1);
  m_myTree->Branch("eta_truthQ1", &eta_truthQ1);
  m_myTree->Branch("phi_truthQ1", &phi_truthQ1);
  m_myTree->Branch("m_truthQ1", &m_truthQ1);
  m_myTree->Branch("pT_truthQ2", &pT_truthQ2);
  m_myTree->Branch("eta_truthQ2", &eta_truthQ2);
  m_myTree->Branch("phi_truthQ2", &phi_truthQ2);
  m_myTree->Branch("m_truthQ2", &m_truthQ2);
  m_myTree->Branch("pT_truthQ3", &pT_truthQ3);
  m_myTree->Branch("eta_truthQ3", &eta_truthQ3);
  m_myTree->Branch("phi_truthQ3", &phi_truthQ3);
  m_myTree->Branch("m_truthQ3", &m_truthQ3);
  m_myTree->Branch("oo1_truth", &oo1_truth);
  m_myTree->Branch("oo2_truth", &oo2_truth);
  m_myTree->Branch("WeightDtilde1", &WeightDtilde1);
  m_myTree->Branch("WeightDtilde2", &WeightDtilde2);


  return StatusCode::SUCCESS;
}

StatusCode ooNNAnalysisAlg::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");

  return StatusCode::SUCCESS;
}

StatusCode ooNNAnalysisAlg::execute() {  
  ATH_MSG_DEBUG ("Executing " << name() << "...");
  // vectors to hold selected objects
  //m_selectedPhotons = std::make_shared<ConstDataVector<xAOD::PhotonContainer>>(SG::VIEW_ELEMENTS);

  ATH_CHECK(fillEventInfo());

  ATH_CHECK(initPDFSet());
  ATH_CHECK(getTruthEvents());
  ATH_CHECK(computeHAWKWeights());

  m_myTree->Fill();
  m_myTree_meta->Fill();

  //if (m_passReconEventSelection)
  //  m_myTree->Fill();
  //if (!m_isData)
  //  m_myTree_meta->Fill();
  //ATH_CHECK(clearVectors());
  return StatusCode::SUCCESS;
}

StatusCode ooNNAnalysisAlg::beginInputFile() { 
  return StatusCode::SUCCESS;
}

StatusCode ooNNAnalysisAlg::fillEventInfo()
{
  SG::ReadHandle<xAOD::EventInfo> eventInfo(m_eventInKey);
  ATH_CHECK(eventInfo.isValid());

  m_mcWeight = eventInfo->mcEventWeights()[0];
  m_mcChannelNumber = eventInfo->mcChannelNumber();
  m_mu = eventInfo->averageInteractionsPerCrossing();
  m_runNumber = eventInfo->runNumber();
  m_eventNumber = eventInfo->eventNumber();
  m_beamSpotMean = eventInfo->beamPosZ();
  m_beamSpotSigma = eventInfo->beamPosSigmaZ();
  m_PileupWeight = 1;
  m_finalWeight = m_mcWeight * m_PileupWeight;

  return StatusCode::SUCCESS;
}

StatusCode ooNNAnalysisAlg::clearVectors() { 
  //m_Vtx_sumPt2.clear();
  return StatusCode::SUCCESS;
}

StatusCode ooNNAnalysisAlg::initPDFSet()
{
  ooES.initPDFSet("CT10", 0, 91.2);
  return StatusCode::SUCCESS;
}

const xAOD::TruthParticleContainer* ooNNAnalysisAlg::getTruthParticles()
{    
  //read container
  SG::ReadHandle<xAOD::TruthParticleContainer> truthParticlesHandle(m_truthParticleContKey);
  if (!truthParticlesHandle.isValid()) {
    ATH_MSG_FATAL("No TruthParticles");
  }
  const xAOD::TruthParticleContainer* truthParticles = truthParticlesHandle.cptr();

  return truthParticles;
}

bool ooNNAnalysisAlg::isFinalHiggs(const xAOD::TruthParticle *part)
{
  if (part == nullptr) { ATH_MSG_FATAL("isFinalHiggs FATAL: particle is NULL"); }
  if (!part->isHiggs()) { return false; }
  if (part->child() == nullptr) { return false; }
  if (part->child()->isHiggs()) { return false; }
  return true;
}

ConstDataVector<xAOD::TruthParticleContainer> ooNNAnalysisAlg::getFinalHiggsBosons(const xAOD::TruthParticleContainer *truthParticles)
{
  if (truthParticles == nullptr) { 
    ATH_MSG_FATAL("getFinalHiggsBosons FATAL: truthPtcls is NULL (probably not saved in input sample)"); 
  }

  ConstDataVector<xAOD::TruthParticleContainer> higgs(SG::VIEW_ELEMENTS);

  for (auto part : *truthParticles) {
    if (isFinalHiggs(part))
    { higgs.push_back(part); }
  }

  return higgs;
}

const xAOD::TruthParticleContainer ooNNAnalysisAlg::getHiggsBosons()
{
  xAOD::TruthParticleContainer cont;

  // Get all truth particles, then good muons, from truth record
  const xAOD::TruthParticleContainer* truthParticles = getTruthParticles();

  if (truthParticles == nullptr) {
    ATH_MSG_FATAL("No TruthParticles, exiting!");
  }

  ConstDataVector<xAOD::TruthParticleContainer> truthcont = getFinalHiggsBosons(&*truthParticles);
  cont = getDeepCopyTruthParticle(truthcont.asDataVector());

  // Make a deep copy, sort by pT  
  cont.sort(comparePt);

  // Add decorations
  for (auto part : cont) {
    part->auxdecor<float>("pt") = part->pt();
    part->auxdecor<float>("rapidity") = part->rapidity();
  }

  return cont;
}

DataVector<xAOD::TruthParticle> ooNNAnalysisAlg::getDeepCopyTruthParticle(const DataVector<xAOD::TruthParticle> *parts)
{
  // Create the new container and its auxilliary store
  DataVector<xAOD::TruthParticle> *copy    = new DataVector<xAOD::TruthParticle>();
  xAOD::AuxContainerBase *copyAux = new xAOD::AuxContainerBase();
  copy->setStore(copyAux);

  for (auto part : *parts) {
    // Copy to output container
    xAOD::TruthParticle *element = new xAOD::TruthParticle();
    copy->push_back(element);
    *element = *part;
    xAOD::setOriginalObjectLink(*part, *element);
  }
  // Make a container for returning
  DataVector<xAOD::TruthParticle> cont(copy->begin(),copy->end(),SG::VIEW_ELEMENTS);

  return cont;
}

/*xAOD::JetContainer applyJetSelection(xAOD::JetContainer* jets)
{
  int jetMinPt = 25000;
  double jetMaxRapidity = 4.40;
  xAOD::JetContainer selected(SG::VIEW_ELEMENTS);

  for (auto jet : jets) {
    // Pt cuts
    if (jet->pt() < jetMinPt) { continue; }

    // Eta cuts
    if (fabs(jet->rapidity()) > jetMaxRapidity) { continue; }

    // Passed cuts, add to selected container
    selected.push_back(jet);
  }

  return selected;
}
*/

/*void ooNNAnalysisAlg::decorateBJetSelection(xAOD::JetContainer* jets)
{  
  SG::AuxElement::Accessor<char> accIsBjet("IsBjet");

  for (auto jet : jets) {
    double deltaRtoClosestB = 999.;

    if (jet->getAttribute("TruthLabelDeltaR_B", deltaRtoClosestB)) {
      if (deltaRtoClosestB >= 0.4) {
        accIsBjet(*jet) = false;
      } else {
        accIsBjet(*jet) = true;
      }
    } else {
      ATH_MSG_FATAL("#BTAG# No TruthInfo ! Cannot run in reference mode !");
      return;
    }
  }
}

void ooNNAnalysisAlg::decorateCJetSelection(xAOD::JetContainer* jets)
{
  SG::AuxElement::Accessor<char> accIsCjet("IsCjet");

  for (auto jet : jets) {
    double deltaRtoClosestC = 999.;

    if (jet->getAttribute("TruthLabelDeltaR_C", deltaRtoClosestC)) {
      if (deltaRtoClosestC >= 0.4) {
        accIsCjet(*jet) = false;
      } else {
        accIsCjet(*jet) = true;
      }
    } else {
      ATH_MSG_FATAL("#CTAG# No TruthInfo ! Cannot run in reference mode !");
      return;
    }
  }
}
*/
bool ooNNAnalysisAlg::comparePt(const xAOD::IParticle *a, const xAOD::IParticle *b)
{ return a->pt() > b->pt(); }

xAOD::JetContainer ooNNAnalysisAlg::getJets()
{
  //read container
  SG::ReadHandle<xAOD::JetContainer> jetContHandle(m_truthJetContKey);
  if (!jetContHandle.isValid()) {
    ATH_MSG_FATAL("Cannot access AntiKt10TruthJets, exiting.");
  }
  //SG::ReadHandle<xAOD::TrackParticleContainer> idTracks_container_handle(m_idTrackParticleContainerKey, ctx);
  //const xAOD::TrackParticleContainer* idTracks = idTracks_container_handle.cptr();  

  // Add the isBjet decoration for use by b-jet functions
  //decorateBJetSelection(&*jets);
  //decorateCJetSelection(&*jets);

  xAOD::JetContainer jets = *jetContHandle;
  jets.sort(comparePt);
  
  return jets;
}

StatusCode ooNNAnalysisAlg::getTruthEvents()
{
  //read container
  SG::ReadHandle<xAOD::TruthEventContainer> truthEventsHandle(m_truthEventContKey);
  if (!truthEventsHandle.isValid()) {
    ATH_MSG_FATAL("Cannot access TruthEvents, exiting.");
  }
  if (truthEventsHandle->size() < 1) {
    ATH_MSG_FATAL("TruthHandler::getTruthEvents() : No TruthEvents?");
  }

  const xAOD::TruthEventContainer* truthEvents = truthEventsHandle.cptr();

  truthEvents->at(0)->pdfInfoParameter(Q, xAOD::TruthEvent::Q);
  truthEvents->at(0)->pdfInfoParameter(Bjorkenx1, xAOD::TruthEvent::X1);
  truthEvents->at(0)->pdfInfoParameter(Bjorkenx2, xAOD::TruthEvent::X2);
  truthEvents->at(0)->pdfInfoParameter(f_q1In, xAOD::TruthEvent::PDGID1);
  truthEvents->at(0)->pdfInfoParameter(f_q2In, xAOD::TruthEvent::PDGID2);

  // get truth higgs and jets
  xAOD::TruthParticleContainer truth_higgs = getHiggsBosons();
  xAOD::JetContainer truth_jets = getJets();

  if(truth_higgs.size()>0){
    E_truthH = truth_higgs[0]->e();
    Px_truthH = truth_higgs[0]->px();
    Py_truthH = truth_higgs[0]->py();
    Pz_truthH = truth_higgs[0]->pz();
  }else{
    E_truthH = 0.;
    Px_truthH = 0.;
    Py_truthH = 0.;
    Pz_truthH = 0.;
  }

  TLorentzVector p_tmp;
  TLorentzVector t_j_p[3];
  for(uint i = 0; i<3; i++){
    if(i<truth_jets.size()) {
      p_tmp.SetPtEtaPhiM(truth_jets[i]->pt(),truth_jets[i]->eta(),truth_jets[i]->phi(),truth_jets[i]->m());
      t_j_p[i] = p_tmp;
    }else{
      p_tmp.SetPtEtaPhiM(0.,0.,0.,0.);
      t_j_p[i] = p_tmp;
    }
  }

  pT_truthQ1 = t_j_p[0].Pt();
  eta_truthQ1 = t_j_p[0].Eta();
  phi_truthQ1 = t_j_p[0].Phi();
  m_truthQ1 = t_j_p[0].M();
  //Pid_truthQ1 = (t_j_id[0]==21) ? 0 : t_j_id[0];
  pT_truthQ2 = t_j_p[1].Pt();
  eta_truthQ2 = t_j_p[1].Eta();
  phi_truthQ2 = t_j_p[1].Phi();
  m_truthQ2 = t_j_p[1].M();
  //Pid_truthQ2 = (t_j_id[1]==21) ? 0 : t_j_id[1];
  pT_truthQ3 = t_j_p[2].Pt();
  eta_truthQ3 = t_j_p[2].Eta();
  phi_truthQ3 = t_j_p[2].Phi();
  m_truthQ3 = t_j_p[2].M();

  return StatusCode::SUCCESS;
}

void ooNNAnalysisAlg::ReweightTruthInfo(const xAOD::TruthParticleContainer *truth_cont, std::vector<xAOD::TruthParticle *> &truth_partons, xAOD::TruthParticle *&truth_higgs)
{

  std::vector<int> pdgId_check;
  pdgId_check.push_back(1);
  pdgId_check.push_back(2);
  pdgId_check.push_back(3);
  pdgId_check.push_back(4);
  pdgId_check.push_back(5);
  pdgId_check.push_back(21);

  for (unsigned int i = 0; i < truth_cont->size(); ++i) {
    //std::cout<<"truth rw_truthPars pt:"<<truth_cont->at(i)->pt()<<std::endl;
    if (truth_cont->at(i)->pdgId() == 25 && truth_cont->at(i)->status() == 22) {
      truth_higgs = const_cast<xAOD::TruthParticle *>(truth_cont->at(i));
      //std::cout<<"higgs pt:"<<truth_higgs->pt()<<std::endl;
    }

    if (std::find(pdgId_check.begin(), pdgId_check.end(), fabs(truth_cont->at(i)->pdgId())) != pdgId_check.end() 
    //(truth_cont->at(i)->status() == 23 || truth_cont->at(i)->status() == 21)) {
    &&(truth_cont->at(i)->status() == 23)) {
      xAOD::TruthParticle *m_parton = const_cast<xAOD::TruthParticle *>(truth_cont->at(i));
      //std::cout<<i<<" parton pt:"<<m_parton->pt()<<std::endl;std::cout<<i<<" parton status:"<<m_parton->status()<<std::endl;std::cout<<i<<" parton pz:"<<m_parton->pz()<<std::endl;
      truth_partons.push_back(m_parton);
    }
  }
}

StatusCode ooNNAnalysisAlg::computeHAWKWeights(){
  double ecm = 13000;                           //proton-proton center-of mass energy in GeV
  double mH = 124.999901;                       //mass of Higgs boson in Gev

  // computing Hawk weights
  double x1 = Bjorkenx1;
  double x2 = Bjorkenx2;

  int flavour1In= (f_q1In == 21) ? 0 : f_q1In;
  int flavour2In= (f_q2In == 21) ? 0 : f_q2In;

  const xAOD::TruthParticleContainer* rw_truthPars = getTruthParticles();
  std::vector<xAOD::TruthParticle *> rw_partons;
  rw_partons.clear();
  xAOD::TruthParticle *rw_higgs = nullptr;

  ReweightTruthInfo(rw_truthPars, rw_partons, rw_higgs);
  int n_out_partons = rw_partons.size();
  double pjet[3][4];
  int out_pdg[3];

  for (int i = 0; i < 3; i++) {
    if (i < n_out_partons) {
      //pjet[i].SetPtEtaPhiM(rw_partons[i]->pt(), rw_partons[i]->eta(), rw_partons[i]->phi(), rw_partons[i]->m());
      pjet[i][0] = rw_partons[i]->e();
      pjet[i][1] = rw_partons[i]->px();
      pjet[i][2] = rw_partons[i]->py();
      pjet[i][3] = rw_partons[i]->pz();
      out_pdg[i] = (rw_partons[i]->pdgId() == 21) ? 0 : rw_partons[i]->pdgId();
    } else {
      //pjet[i].SetPtEtaPhiM(0, 0, 0, 0);
      pjet[i][0] = 0.;
      pjet[i][1] = 0.;
      pjet[i][2] = 0.;
      pjet[i][3] = 0.;
      out_pdg[i] = -99;
    }
  }

  int npafin= rw_partons.size();

  int flavour1Out= out_pdg[0];
  int flavour2Out= out_pdg[1];
  int flavour3Out= out_pdg[2];

  double h_e= E_truthH;
  double h_px= Px_truthH;
  double h_py= Py_truthH;
  double h_pz= Pz_truthH;


  double oj1_e= pjet[0][0];
  double oj1_px= pjet[0][1];
  double oj1_py= pjet[0][2];
  double oj1_pz= pjet[0][3];
  double oj2_e= pjet[1][0];
  double oj2_px= pjet[1][1];
  double oj2_py= pjet[1][2];
  double oj2_pz= pjet[1][3];
  double oj3_e= pjet[2][0];
  double oj3_px= pjet[2][1];
  double oj3_py= pjet[2][2];
  double oj3_pz= pjet[2][3];

  double phiggs[] = {h_e/1000,h_px/1000,h_py/1000,h_pz/1000};

  double pjet1[] = {oj1_e/1000,oj1_px/1000,oj1_py/1000,oj1_pz/1000};
  double pjet2[] = {oj2_e/1000,oj2_px/1000,oj2_py/1000,oj2_pz/1000};
  double pjet3[] = {oj3_e/1000,oj3_px/1000,oj3_py/1000,oj3_pz/1000};

  //std::pair<double,double> oo, weights;
  double w1 = ooES.getWeightsDtilde(0, m_eventNumber, ecm, mH , npafin,flavour1In,flavour2In,flavour1Out,flavour2Out,flavour3Out,x1,x2,pjet1,pjet2,pjet3,phiggs); // mcID of PowhegPy8EG_NNPDF30_AZNLOCTEQ6L1_VBFH125_gamgam
  double w2 = ooES.getWeightsDtilde(1, m_eventNumber, ecm, mH , npafin,flavour1In,flavour2In,flavour1Out,flavour2Out,flavour3Out,x1,x2,pjet1,pjet2,pjet3,phiggs);

  //std::cout<<"Calling getWeightsDtilde(...)! Results are: "<< w1 << " , " << w2 <<std::endl;
  double t_oo1 = ooES.getOptObs(0, m_eventNumber, ecm, mH ,x1,x2,Q,pjet1,pjet2,phiggs);
  double t_oo2 = ooES.getOptObs(1, m_eventNumber, ecm, mH ,x1,x2,Q,pjet1,pjet2,phiggs);

  oo1_truth = t_oo1;
  oo2_truth = t_oo2;
  WeightDtilde1 = w1;
  WeightDtilde2 = w2;

  return StatusCode::SUCCESS;
}
